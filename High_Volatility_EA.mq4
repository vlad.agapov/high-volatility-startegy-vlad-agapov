extern int Magic_Number  = 20050611;
extern bool trading_is_ON = true;
extern int max_trades_per_symbol = 1;
extern double trade_size = 0.1;
extern int N_pips_travelled = 50;
extern int TakeProfit = 200;
extern int StopLoss  = 100;

bool starting_point_defined = false;
double starting_point;
int number_of_orders_previous;

int init()
{
   number_of_orders_previous = 0;
   if (starting_point_defined == false)
   {
      starting_point = NormalizeDouble((Bid+Ask)/2,Digits);
      starting_point_defined = true;
   }
   return(0);  
}

//+------------------------------------------------------------------+
//| Calculate open positions                                         |
//+------------------------------------------------------------------+
int CalculateCurrentOrders()
{
   int buys=0,sells=0;
//----
   for(int i=0;i<OrdersTotal();i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==Magic_Number )
        {
         if(OrderType()==OP_BUY)  buys++;
         if(OrderType()==OP_SELL) sells++;
        }
     }
   return(buys+MathAbs(sells));
}
//+------------------------------------------------------------------+
//| Calculate optimal lot size                                       |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Check for open order conditions                                  |
//+------------------------------------------------------------------+
void CheckForOpen()
{
   int order_sent_check;
   if(CalculateCurrentOrders()>=max_trades_per_symbol) return;
   
//---- sell conditions
   if(starting_point - Bid > N_pips_travelled*Point())
   {
      order_sent_check = OrderSend(Symbol(),OP_SELL,trade_size,Bid,3,Bid+StopLoss*Point,Bid-TakeProfit*Point,"",Magic_Number ,0,Red);
      number_of_orders_previous = CalculateCurrentOrders();
      return;
   }
//---- buy conditions
   if(Ask - starting_point > N_pips_travelled*Point())  
   {
      order_sent_check = OrderSend(Symbol(),OP_BUY,trade_size,Ask,3,Ask-StopLoss*Point,Ask+TakeProfit*Point,"",Magic_Number,0,Blue);
      number_of_orders_previous = CalculateCurrentOrders();
      return;
   }
//----
}
//+------------------------------------------------------------------+
//| Check for close order conditions                                 |
//+------------------------------------------------------------------+
void CheckForStartingPoint()
{
   if (CalculateCurrentOrders()<number_of_orders_previous&&starting_point_defined == true)
   {
      starting_point = NormalizeDouble((Bid+Ask)/2,Digits);
   }
   number_of_orders_previous = CalculateCurrentOrders();
   return;
}

//+------------------------------------------------------------------+
//| Start function                                                   |
//+------------------------------------------------------------------+
void start()
{
   if(Bars<100 ) return;
   Print(starting_point);
   if (trading_is_ON)
   {
      CheckForStartingPoint();
      if (CalculateCurrentOrders()==0)
      {
         CheckForOpen();
      }
   }
//----
}